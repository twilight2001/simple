from flask import Flask
from flask_restx import Resource, Api
from flask_cors import CORS, cross_origin

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world v2'}

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)